#include "aboutwin.h"
#include "ui_aboutwin.h"

AboutWin::AboutWin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutWin)
{
    ui->setupUi(this);
}

AboutWin::~AboutWin()
{
    delete ui;
}

void AboutWin::on_pushButton_clicked()
{
    AboutWin::destroy();
}
