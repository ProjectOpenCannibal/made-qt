#-------------------------------------------------
#
# Project created by QtCreator 2013-04-07T11:28:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MADE
TEMPLATE = app


SOURCES += main.cpp\
        made.cpp \
    aboutwin.cpp

HEADERS  += made.h \
    aboutwin.h

FORMS    += made.ui \
    aboutwin.ui
