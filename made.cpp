#include "made.h"
#include "ui_made.h"
#include <stdlib.h>
#include <stdio.h>
#include "aboutwin.h"
#include <QFileDialog>
#include <QString>
#include <QFile>
#include <QPlainTextEdit>
#include <QTextStream>
#include <QTreeView>
#include <QDirModel>
#include <QShowEvent>
#include <QDebug>
#include <QBool>
#include <QFontMetrics>
#include <QMessageBox>

QString theFile;
QString origWinTitle;
QDirModel model;
bool isFileSaved;
bool isUserNotifiedFileNotSaved;
//QTreeView fileTree;

MADE::MADE(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MADE)
{
    ui->setupUi(this);
}

MADE::~MADE()
{
    delete ui;
}

void MADE::showEvent( QShowEvent * event )
{
   refreshTree();
   QList<int> sizes;
   sizes << 250 << 1000;
   ui->splitter->setSizes(sizes);
   const int tabStop = 4;
   QFontMetrics metrics(ui->textEdit->font());
   ui->textEdit->setTabStopWidth(tabStop * metrics.width(' '));
   isFileSaved = true;
   isUserNotifiedFileNotSaved = false;
}

void MADE::on_actionQuit_triggered()
{
    /* check if user needs to save documents */
    if (isFileSaved == false)
    {
        // throw dialog asking if user wants to save
        promptUser(QUITPROMPT);
    } else {
        exit(EXIT_SUCCESS);
    }
}

void MADE::on_actionAbout_triggered()
{
    // show about window
    AboutWin *aboutWin;
    aboutWin = new AboutWin(this);
    aboutWin->show();
}

void MADE::on_actionNew_File_triggered()
{
    // new file
    if (isFileSaved == true)
    {
        this->setWindowTitle("Untitled");
        ui->textEdit->setPlainText(NULL);
        theFile = "";
        isFileSaved = false;
    } else {
        // TODO: ask if user wants to save current file
        // throw dialog asking if user wants to save
        promptUser(NEWFILEPROMPT);
    }
}

void MADE::on_actionOpen_File_triggered()
{
    if (isFileSaved == true)
    {
        // open file
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
        QFile inputFile(fileName);
        //ui->textEdit->settext(fileName);
        inputFile.open(QIODevice::ReadOnly);

        QTextStream in(&inputFile);
        QString line = in.readAll();
        inputFile.close();
        ui->textEdit->setPlainText(line);
        QFileInfo fileInfo = inputFile.fileName();
        theFile = fileInfo.filePath();
        this->setWindowTitle(fileInfo.fileName());
        origWinTitle = this->windowTitle();
        isFileSaved = true;
        isUserNotifiedFileNotSaved = false;
    } else {
        promptUser(OPENPROMPT);
    }

}

void MADE::on_actionSave_triggered()
{
    // save file
    saveFile();
}

void MADE::on_treeView_doubleClicked(const QModelIndex &index)
{
    QModelIndexList list = ui->treeView->selectionModel()->selectedIndexes();
    QDirModel* model = (QDirModel*)ui->treeView->model();
    int row = -1;
    foreach (QModelIndex index, list)
    {
        if (index.row() !=row && index.column()==0)
        {
            QFileInfo fileInfo = model->fileInfo(index);
            if (fileInfo.isFile())
            {
                // does the user already have a modified file open?
                if (isFileSaved == true) // no they don't, open the document!
                {
                    // it's a file, open it!
                    theFile = fileInfo.filePath();
                    QFile inputFile(fileInfo.filePath());
                    inputFile.open(QIODevice::ReadWrite);

                    QTextStream in(&inputFile);
                    QString line = in.readAll();
                    inputFile.close();
                    ui->textEdit->setText(line);
                    row = index.row();
                    isFileSaved = true;
                    isUserNotifiedFileNotSaved = false;
                    this->setWindowTitle(fileInfo.fileName());
                    // origWinTitle is used to maintain the original window title. used when text data has been modified
                    origWinTitle = this->windowTitle();
                } else { // yes they do, prompt them!
                    // display prompt
                    QMessageBox msgBox;
                    msgBox.setText("The document has been modified.");
                    msgBox.setInformativeText("Do you want to save it first?");
                    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
                    msgBox.setDefaultButton(QMessageBox::Save);
                    int ret = msgBox.exec();
                    switch (ret) {
                    case QMessageBox::Save:
                        // Save was clicked
                        saveFile();
                        break;
                    case QMessageBox::Discard:
                    {
                        // don't save was clicked, so open the file
                        // it's a file, open it!
                        theFile = fileInfo.filePath();
                        QFile inputFile(fileInfo.filePath());
                        inputFile.open(QIODevice::ReadWrite);

                        QTextStream in(&inputFile);
                        QString line = in.readAll();
                        inputFile.close();
                        ui->textEdit->setText(line);
                        row = index.row();
                        isFileSaved = true;
                        isUserNotifiedFileNotSaved = false;
                        this->setWindowTitle(fileInfo.fileName());
                        // origWinTitle is used to maintain the original window title. used when text data has been modified
                        origWinTitle = this->windowTitle();
                        break;
                    }
                    case QMessageBox::Cancel:
                        // Cancel was clicked
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    }
}

void MADE::on_textEdit_textChanged()
{
    isFileSaved = false;
    if (isUserNotifiedFileNotSaved == false)
    {
        // notify user that file has been modified
        this->setWindowTitle(this->windowTitle() + " (*)");
        isUserNotifiedFileNotSaved = true;
    }
}

void MADE::saveFile()
{
    // save file
    QFile outFile;
    if (theFile == "")
    {
        // new file, needs a name
        QString saveFileName = QFileDialog::getSaveFileName(this, "Save As", "./untitled.txt", tr("files(*.*)"));
        if (saveFileName != NULL)
        {
            outFile.setFileName(saveFileName);
            outFile.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream newOut(&outFile);
            newOut << ui->textEdit->toPlainText();
            QFileInfo fileInfo = outFile.fileName();
            theFile = fileInfo.filePath();
            this->setWindowTitle(fileInfo.fileName());
            origWinTitle = this->windowTitle();
            isFileSaved = true;
            isUserNotifiedFileNotSaved = false;
            refreshTree();
        }
    } else {
        outFile.setFileName(theFile);
        outFile.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&outFile);
        out << ui->textEdit->toPlainText();
        this->setWindowTitle(origWinTitle);
        isFileSaved = true;
        isUserNotifiedFileNotSaved = false;
    }
}

void MADE::promptUser(int promptType)
{
    // prompt user depending on promptType
    switch(promptType)
    {
    case QUITPROMPT:
    {
        // quit prompt
        QMessageBox quitMsgBox;
        quitMsgBox.setText("The document has been modified.");
        quitMsgBox.setInformativeText("Do you want to save it?");
        quitMsgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        quitMsgBox.setDefaultButton(QMessageBox::Save);
        int quitRet = quitMsgBox.exec();
        switch (quitRet) {
            case QMessageBox::Save:
                //Save was clicked
                saveFile();
                break;
            case QMessageBox::Discard:
                //Don't Save was clicked
                exit(EXIT_SUCCESS);
                break;
            case QMessageBox::Cancel:
                // Cancel was clicked
                break;
            default:
                // should never be reached
                break;
        }
        break;
    }
    case NEWFILEPROMPT:
    {
        // new file prompt
        QMessageBox msgBox;
        msgBox.setText("The document has been modified.");
        msgBox.setInformativeText("Do you want to save it?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();
        switch (ret) {
            case QMessageBox::Save:
                //Save was clicked
                saveFile();
                this->setWindowTitle("Untitled");
                ui->textEdit->setPlainText(NULL);
                theFile = "";
                refreshTree();
                break;
            case QMessageBox::Discard:
                //Don't Save was clicked
                this->setWindowTitle("Untitled");
                ui->textEdit->setPlainText(NULL);
                theFile = "";
                break;
            case QMessageBox::Cancel:
                // Cancel was clicked
                break;
            default:
                // should never be reached
                break;
        }
        break;
    }
    case OPENPROMPT:
    {
        QMessageBox msgBox;
        msgBox.setText("The document has been modified.");
        msgBox.setInformativeText("Do you want to save it?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();
        switch (ret) {
        case QMessageBox::Save:
            // Save was clicked
            saveFile();
            break;
        case QMessageBox::Discard:
        {
            // open file
            QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
            QFile inputFile(fileName);
            //ui->textEdit->settext(fileName);
            inputFile.open(QIODevice::ReadOnly);

            QTextStream in(&inputFile);
            QString line = in.readAll();
            inputFile.close();
            ui->textEdit->setPlainText(line);
            QFileInfo fileInfo = inputFile.fileName();
            theFile = fileInfo.filePath();
            this->setWindowTitle(fileInfo.fileName());
            origWinTitle = this->windowTitle();
            isFileSaved = true;
            isUserNotifiedFileNotSaved = false;
        }
        case QMessageBox::Cancel:
            // do nothing
            break;
        }
    }
    }
}

void MADE::refreshTree()
{
    ui->treeView->setModel(NULL);
    ui->treeView->setModel(&model);
    ui->treeView->setRootIndex(model.index(QDir::rootPath()));
    ui->treeView->setColumnHidden( 1, true );
    ui->treeView->setColumnHidden( 2, true );
    ui->treeView->setColumnHidden( 3, true );
}
