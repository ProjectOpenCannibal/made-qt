#ifndef ABOUTWIN_H
#define ABOUTWIN_H

#include <QDialog>

namespace Ui {
class AboutWin;
}

class AboutWin : public QDialog
{
    Q_OBJECT
    
public:
    explicit AboutWin(QWidget *parent = 0);
    ~AboutWin();
    
private slots:
    void on_pushButton_clicked();

private:
    Ui::AboutWin *ui;
};

#endif // ABOUTWIN_H
