#ifndef MADE_H
#define MADE_H

#include <QMainWindow>
#include <QModelIndex>
#include <QModelIndexList>

#ifndef FILEPROMPTS
#define QUITPROMPT 0
#define NEWFILEPROMPT 1
#define OPENPROMPT 2
#endif // FILEPROMPTS

namespace Ui {
class MADE;
}

class MADE : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MADE(QWidget *parent = 0);
    ~MADE();
    void showEvent( QShowEvent * event );
    void saveFile();
    void promptUser(int promptType);
    void refreshTree();
    
private slots:
    void on_actionQuit_triggered();

    void on_actionAbout_triggered();

    void on_actionNew_File_triggered();

    void on_actionOpen_File_triggered();

    void on_actionSave_triggered();

    void on_treeView_doubleClicked(const QModelIndex &index);

    void on_textEdit_textChanged();

private:
    Ui::MADE *ui;
};

#endif // MADE_H
